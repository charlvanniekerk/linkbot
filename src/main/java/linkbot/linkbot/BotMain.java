package linkbot.linkbot;

import org.pircbotx.PircBotX;

public class BotMain {
    public static void main(String[] args) {
        BotConfigurationHelper.buildConfigurationList().forEach(
                configuration -> new Thread(new BotRunner(new PircBotX(configuration))).start()
        );
    }
}
