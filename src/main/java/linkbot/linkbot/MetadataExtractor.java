package linkbot.linkbot;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.entity.ContentType;
import org.apache.http.impl.client.HttpClients;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.text.DecimalFormat;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Locale;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;

/**
 * Helper class to extract metadata from the resource behind a particular URL
 */
class MetadataExtractor {
    private static final Logger LOGGER = Logger.getLogger(MetadataExtractor.class.getName());

    private static final String AUTHOR_NAME_ATTRIBUTE = "content";
    private static final String AUTHOR_NAME_SELECTOR = "span[itemprop=author] link[itemprop=name]";
    private static final String DATE_PUBLISHED_ATTRIBUTE = "content";
    private static final String DATE_PUBLISHED_SELECTOR = "meta[itemprop=datePublished]";
    private static final String DIMENSION_DELIMITER = "x";
    private static final String DURATION_ATTRIBUTE = "content";
    private static final String DURATION_SELECTOR = "meta[itemprop=duration]";
    private static final String EXCEPTION_ERROR_FETCHING = "Error fetching: ";
    private static final String GENRE_ATTRIBUTE = "content";
    private static final String GENRE_SELECTOR = "meta[itemprop=genre]";
    private static final String INTERACTION_COUNT_ATTRIBUTE = "content";
    private static final String INTERACTION_COUNT_SELECTOR = "meta[itemprop=interactionCount]";
    private static final String MIME_TYPE_SEPARATOR = "/";
    private static final String OG_TITLE_ATTRIBUTE = "content";
    private static final String OG_TITLE_SELECTOR = "meta[property=og:title]";
    private static final String RECEIVED_STATUS_CODE = "Received status code %s from %s";
    private static final String RECEIVED_UNKNOWN_MIME_TYPE = "Unable to extract metadata for MIME type: ";
    private static final String RESOURCE_BUNDLE = "messages";
    private static final String RESOURCE_COMMENTS = "comments";
    private static final String RESOURCE_DATE = "date";
    private static final String RESOURCE_DOWNLOADS = "downloads";
    private static final String RESOURCE_DURATION_HOURS = "duration.hours";
    private static final String RESOURCE_DURATION_MINUTES = "duration.minutes";
    private static final String RESOURCE_INTERACTION_COUNT = "interactionCount";
    private static final String RESOURCE_PLAYS = "plays";
    private static final String RESPONSE_DELIMITER = " :: ";
    private static final String SOUNDCLOUD_COMMENTS_COUNT_ATTRIBUTE = "content";
    private static final String SOUNDCLOUD_COMMENTS_COUNT_SELECTOR = "meta[property=soundcloud:comments_count]";
    private static final String SOUNDCLOUD_DOWNLOAD_COUNT_ATTRIBUTE = "content";
    private static final String SOUNDCLOUD_DOWNLOAD_COUNT_SELECTOR = "meta[property=soundcloud:download_count]";
    private static final String SOUNDCLOUD_PLAY_COUNT_ATTRIBUTE = "content";
    private static final String SOUNDCLOUD_PLAY_COUNT_SELECTOR = "meta[property=soundcloud:play_count]";
    private static final String TIME_PUBDATE_SELECTOR = "time[pubdate]";
    private static final String TITLE_ATTRIBUTE = "content";
    private static final String TITLE_SELECTOR = "meta[name=title]";
    private static final String WHITESPACE_CHARACTERS = "[\\t\\n\\r ]+";
    private static final int PLAIN_TEXT_MAX_LENGTH = 80;

    private final Locale locale;
    private final ResourceBundle resourceBundle;
    private final String url;

    /**
     * Creates a new MetadataExtractor object
     * @param url The URL of the resource
     * @param locale The locale to use for message formatting
     */
    public MetadataExtractor(String url, Locale locale) {
        this.locale = locale;
        this.resourceBundle = ResourceBundle.getBundle(RESOURCE_BUNDLE, locale);
        this.url = url;

        LOGGER.info(url);
    }

    /**
     * Extracts metadata from the resource behind a particular URL
     * @return The formatted metadata strings
     */
    String extractMetadata() {
        try (
                var httpclient = HttpClients.createDefault();
                var response = httpclient.execute(new HttpGet(this.url))
        ) {
            var statusCode = response.getStatusLine().getStatusCode();

            if (statusCode == HttpStatus.SC_OK) {
                return fetchMetadatafromHttpEntity(response.getEntity(), this.url);
            }

            return String.format(RECEIVED_STATUS_CODE, statusCode, this.url);
        } catch (BotException | IOException exception) {
            LOGGER.log(Level.SEVERE, EXCEPTION_ERROR_FETCHING + url, exception);

            return null;
        }
    }

    /**
     * Attempts to fetch metadata from the given HTTP entity.
     * @param httpEntity The HTTP entity to fetch metadata from
     * @param fetchURL The URL the entity was retrieved from
     * @return The fetched metadata
     * @throws IOException If an input/output exception occurs
     */
    private String fetchMetadatafromHttpEntity(HttpEntity httpEntity, String fetchURL) throws IOException {
        var contentType = ContentType.get(httpEntity);
        var mimeType = contentType.getMimeType();
        var charset = contentType.getCharset();

        try (InputStream content = httpEntity.getContent()) {
            if (ContentType.IMAGE_JPEG.getMimeType().equals(mimeType)
                    || ContentType.IMAGE_PNG.getMimeType().equals(mimeType)
                    || ContentType.IMAGE_GIF.getMimeType().equals(mimeType)) {
                long contentLength = httpEntity.getContentLength();
                return extractImage(content, contentLength, mimeType);
            }

            if (ContentType.TEXT_PLAIN.getMimeType().equals(mimeType)) {
                var text = IOUtils.toString(content, charset).trim().replaceAll(WHITESPACE_CHARACTERS, StringUtils.SPACE);
                return StringUtils.abbreviate(text, PLAIN_TEXT_MAX_LENGTH);
            }

            if (ContentType.APPLICATION_XHTML_XML.getMimeType().equals(mimeType)
                    || ContentType.TEXT_HTML.getMimeType().equals(mimeType)) {
                var charsetName = Optional.ofNullable(charset).map(Charset::name).orElse(null);
                return extractHTML(Jsoup.parse(content, charsetName, fetchURL));
            }

            return RECEIVED_UNKNOWN_MIME_TYPE + mimeType;
        }
    }

    /**
     * Generates a text description of an image
     * @param content The contents of the HTTP response
     * @param contentLength THe content length in bytes
     * @param mimeType The MIME type of the HTTP response
     * @return A text description of the image
     * @throws IOException If an input/output exception occurs
     */
    private String extractImage(InputStream content, long contentLength, String mimeType) throws IOException {
        var bufferedImage = ImageIO.read(content);

        return String.join(RESPONSE_DELIMITER,
                mimeType.split(MIME_TYPE_SEPARATOR)[1].toUpperCase(),
                String.join(DIMENSION_DELIMITER,
                        Integer.toString(bufferedImage.getWidth()),
                        Integer.toString(bufferedImage.getHeight())),
                FileUtils.byteCountToDisplaySize(contentLength));
    }

    /**
     * Extracts metadata from an HTMKL document
     * @param document The HTML document
     * @return The metadata string
     */
    private String extractHTML(Document document) {
        var duration = document.select(DURATION_SELECTOR).attr(DURATION_ATTRIBUTE);
        var ogTitle = document.select(OG_TITLE_SELECTOR).attr(OG_TITLE_ATTRIBUTE);
        var soundcloudCommentsCount = document.select(SOUNDCLOUD_COMMENTS_COUNT_SELECTOR).attr(SOUNDCLOUD_COMMENTS_COUNT_ATTRIBUTE);
        var soundcloudDownloadCount = document.select(SOUNDCLOUD_DOWNLOAD_COUNT_SELECTOR).attr(SOUNDCLOUD_DOWNLOAD_COUNT_ATTRIBUTE);
        var soundcloudPlayCount = document.select(SOUNDCLOUD_PLAY_COUNT_SELECTOR).attr(SOUNDCLOUD_PLAY_COUNT_ATTRIBUTE);
        var timePubdate = document.select(TIME_PUBDATE_SELECTOR).first();

        if (!duration.isEmpty() && !ogTitle.isEmpty() && timePubdate != null && !soundcloudCommentsCount.isEmpty() && !soundcloudDownloadCount.isEmpty() && !soundcloudPlayCount.isEmpty()) {
            duration = transformDuration(duration);
            soundcloudCommentsCount = String.format(this.resourceBundle.getString(RESOURCE_COMMENTS), transformLong(soundcloudCommentsCount));
            soundcloudDownloadCount = String.format(this.resourceBundle.getString(RESOURCE_DOWNLOADS), transformLong(soundcloudDownloadCount));
            soundcloudPlayCount = String.format(this.resourceBundle.getString(RESOURCE_PLAYS), transformLong(soundcloudPlayCount));
            var timePubDateText = transformDate(timePubdate.text(), DateTimeFormatter.ISO_DATE_TIME);

            return String.join(RESPONSE_DELIMITER, ogTitle, duration, timePubDateText, soundcloudPlayCount, soundcloudCommentsCount, soundcloudDownloadCount);
        }

        var authorName = document.select(AUTHOR_NAME_SELECTOR).attr(AUTHOR_NAME_ATTRIBUTE);
        var datePublished = document.select(DATE_PUBLISHED_SELECTOR).attr(DATE_PUBLISHED_ATTRIBUTE);
        var genre = document.select(GENRE_SELECTOR).attr(GENRE_ATTRIBUTE);
        var interactionCount = document.select(INTERACTION_COUNT_SELECTOR).attr(INTERACTION_COUNT_ATTRIBUTE);
        var title = document.select(TITLE_SELECTOR).attr(TITLE_ATTRIBUTE);

        if (!authorName.isEmpty() && !datePublished.isEmpty() && !genre.isEmpty() && !interactionCount.isEmpty() && !title.isEmpty()) {
            datePublished = transformDate(datePublished, DateTimeFormatter.ISO_DATE);
            interactionCount = String.format(this.resourceBundle.getString(RESOURCE_INTERACTION_COUNT), transformLong(interactionCount));

            return String.join(RESPONSE_DELIMITER, title, authorName, datePublished, genre, interactionCount);
        }

        return document.title();
    }

    /**
     * Formats a string containing a long integer to a more human readable string
     * @param string The string containing the unformatted long
     * @return The human readable string
     */
    private String transformLong(String string) {
        return DecimalFormat.getInstance(this.locale).format(Long.parseLong(string));
    }

    /**
     * Formats a string containing a date to a more human readable string
     * @param string The string containing the unformatted date
     * @param dateTimeFormatter The formatter to use to parse the given string
     * @return The formatted date
     */
    private String transformDate(String string, DateTimeFormatter dateTimeFormatter) {
        return DateTimeFormatter.ofPattern(this.resourceBundle.getString(RESOURCE_DATE), this.locale)
                .format(LocalDate.parse(string, dateTimeFormatter));
    }

    /**
     * Formats a string containing a duration to a more human readable string
     * @param string The ISO-8601 duration
     * @return The formatted duration
     */
    private String transformDuration(String string) {
        var localTime = LocalTime.MIDNIGHT.plus(Duration.parse(string));
        var format = this.resourceBundle.getString(localTime.getHour() == 0 ? RESOURCE_DURATION_MINUTES : RESOURCE_DURATION_HOURS);

        return DateTimeFormatter.ofPattern(format, this.locale).format(localTime);
    }
}
