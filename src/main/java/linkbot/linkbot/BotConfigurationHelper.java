package linkbot.linkbot;

import org.pircbotx.Configuration;
import org.pircbotx.UtilSSLSocketFactory;
import org.yaml.snakeyaml.Yaml;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import static java.util.stream.Collectors.toList;

/**
 * Helper class to load and parse the bot configuration after starting up
 */
class BotConfigurationHelper {
    private static final String AUTO_RECONNECT_ATTEMPTS_PROPERTY_NAME = "autoReconnectAttempts";
    private static final String AUTO_RECONNECT_DELAY_PROPERTY_NAME = "autoReconnectDelay";
    private static final String AUTO_RECONNECT_PROPERTY_NAME = "autoReconnect";
    private static final String CHANNELS_PROPERTY_NAME = "channels";
    private static final String CONFIGURATION_FILE_PATH = "/linkbot.yml";
    private static final String COUNTRY_PROPERTY_NAME = "country";
    private static final String FINGER_PROPERTY_NAME = "finger";
    private static final String GLOBAL_PROPERTY_GROUP_NAME = "global";
    private static final String IGNORED_NICKS_PROPERTY_NAME = "ignoredNicks";
    private static final String LANGUAGE_PROPERTY_NAME = "language";
    private static final String LOGIN_PROPERTY_NAME = "login";
    private static final String NAME_PROPERTY_NAME = "name";
    private static final String NICKSERV_NICK_PROPERTY_NAME = "nickServNick";
    private static final String NICKSERV_RESPONSE_PROPERTY_NAME = "nickServResponse";
    private static final String NICKSERV_TRIGGER_PROPERTY_NAME = "nickServTrigger";
    private static final String PORT_PROPERTY_NAME = "port";
    private static final String REAL_NAME_PROPERTY_NAME = "realName";
    private static final String SERVER_PROPERTY_NAME = "server";
    private static final String USE_TLS_PROPERTY_NAME = "useTls";
    private static final String VERIFY_CERTIFICATE_PROPERTY_NAME = "verifyCertificate";
    private static final String VERSION_PROPERTY_NAME = "version";

    /**
     * Builds a list of pircbotx configuration objects
     * @return List of pircbotx configuration objects
     */
    static List<Configuration> buildConfigurationList() {
        return readConfiguration().entrySet().stream().map(BotConfigurationHelper::buildConfiguration).collect(toList());
    }

    /**
     * Builds a pircbotx configuration object from a configuration map
     * @param configurationEntry Entry of name and map of configuration variables
     * @return Pircbotx configuration object
     */
    private static Configuration buildConfiguration(Map.Entry<String, Map<String, Object>> configurationEntry) {
        var builder = new BotConfiguration.Builder();
        var configurationMap = configurationEntry.getValue();

        builder.addAutoJoinChannels((List<String>)configurationMap.get(CHANNELS_PROPERTY_NAME));
        builder.addListener(new BotListener());
        builder.addServer((String)configurationMap.get(SERVER_PROPERTY_NAME), (Integer) configurationMap.get(PORT_PROPERTY_NAME));
        builder.setAutoReconnect((Boolean)configurationMap.get(AUTO_RECONNECT_PROPERTY_NAME));
        builder.setAutoReconnectAttempts((Integer)configurationMap.get(AUTO_RECONNECT_ATTEMPTS_PROPERTY_NAME));
        builder.setAutoReconnectDelay((Integer)configurationMap.get(AUTO_RECONNECT_DELAY_PROPERTY_NAME));
        builder.setFinger((String)configurationMap.get(FINGER_PROPERTY_NAME));
        builder.setIgnoredNicks((List<String>)configurationMap.get(IGNORED_NICKS_PROPERTY_NAME));
        builder.setLocale(new Locale((String)configurationMap.get(LANGUAGE_PROPERTY_NAME), (String)configurationMap.get(COUNTRY_PROPERTY_NAME)));
        builder.setLogin((String)configurationMap.get(LOGIN_PROPERTY_NAME));
        builder.setName((String)configurationMap.get(NAME_PROPERTY_NAME));
        builder.setNetworkName(configurationEntry.getKey());
        builder.setNickServNick((String)configurationMap.get(NICKSERV_NICK_PROPERTY_NAME));
        builder.setNickServResponse((String)configurationMap.get(NICKSERV_RESPONSE_PROPERTY_NAME));
        builder.setNickServTrigger((String)configurationMap.get(NICKSERV_TRIGGER_PROPERTY_NAME));
        builder.setRealName((String)configurationMap.get(REAL_NAME_PROPERTY_NAME));
        builder.setVersion((String)configurationMap.get(VERSION_PROPERTY_NAME));

        if ((Boolean)configurationMap.get(USE_TLS_PROPERTY_NAME)) {
            if ((Boolean)configurationMap.get(VERIFY_CERTIFICATE_PROPERTY_NAME)) {
                builder.setSocketFactory(new UtilSSLSocketFactory());
            } else {
                builder.setSocketFactory(new UtilSSLSocketFactory().trustAllCertificates());
            }
        }

        return builder.buildConfiguration();
    }

    /**
     * Reads configuration and resolve defaults to globals
     * @return Map of configuration variables
     */
    private static Map<String, Map<String, Object>> readConfiguration() {
        Map<String, Map<String, Object>> configuration;

        try (var inputStream = new FileInputStream(CONFIGURATION_FILE_PATH)) {
            configuration = new Yaml().load(inputStream);
        } catch (IOException exception) {
            throw new BotException(exception);
        }

        if (configuration.containsKey(GLOBAL_PROPERTY_GROUP_NAME)) {
            var globalAttributes = configuration.get(GLOBAL_PROPERTY_GROUP_NAME);
            configuration.remove(GLOBAL_PROPERTY_GROUP_NAME);
            configuration.values().forEach(localAttributes ->
                    globalAttributes.entrySet().stream().filter(e -> !localAttributes.containsKey(e.getKey()))
                            .forEach(e -> localAttributes.put(e.getKey(), e.getValue()))
            );
        }

        return configuration;
    }
}
