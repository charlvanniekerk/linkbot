package linkbot.linkbot;

import org.pircbotx.Configuration;

import java.util.List;

/**
 * Class extending pircbotx's configuration class with some extra properties
 */
class BotConfiguration extends Configuration {
    private List<String> ignoredNicks;
    private String networkName;
    private String nickServNick;
    private String nickServResponse;
    private String nickServTrigger;

    private BotConfiguration(Builder builder) {
        super(builder);
        this.ignoredNicks = builder.getIgnoredNicks();
        this.networkName = builder.getNetworkName();
        this.nickServNick = builder.getNickServNick();
        this.nickServResponse = builder.getNickServResponse();
        this.nickServTrigger = builder.getNickServTrigger();
    }

    List<String> getIgnoredNicks() {
        return ignoredNicks;
    }

    String getNetworkName() {
        return networkName;
    }

    String getNickServNick() {
        return nickServNick;
    }

    String getNickServResponse() {
        return nickServResponse;
    }

    String getNickServTrigger() {
        return nickServTrigger;
    }

    static class Builder extends Configuration.Builder {
        private List<String> ignoredNicks;
        private String networkName;
        private String nickServNick;
        private String nickServResponse;
        private String nickServTrigger;

        List<String> getIgnoredNicks() {
            return ignoredNicks;
        }

        void setIgnoredNicks(List<String> ignoreNicks) {
            this.ignoredNicks = ignoreNicks;
        }

        String getNetworkName() {
            return networkName;
        }

        void setNetworkName(String networkName) {
            this.networkName = networkName;
        }

        String getNickServNick() {
            return nickServNick;
        }

        void setNickServNick(String nickServNick) {
            this.nickServNick = nickServNick;
        }

        String getNickServResponse() {
            return nickServResponse;
        }

        void setNickServResponse(String nickServResponse) {
            this.nickServResponse = nickServResponse;
        }

        String getNickServTrigger() {
            return nickServTrigger;
        }

        void setNickServTrigger(String nickServTrigger) {
            this.nickServTrigger = nickServTrigger;
        }

        public BotConfiguration buildConfiguration() {
            return new BotConfiguration(this);
        }
    }
}
