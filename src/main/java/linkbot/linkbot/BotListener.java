package linkbot.linkbot;

import org.apache.commons.lang3.StringUtils;
import org.nibor.autolink.LinkExtractor;
import org.nibor.autolink.LinkType;
import org.pircbotx.hooks.ListenerAdapter;
import org.pircbotx.hooks.events.ConnectEvent;
import org.pircbotx.hooks.events.InviteEvent;
import org.pircbotx.hooks.events.MessageEvent;
import org.pircbotx.hooks.events.NoticeEvent;

import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.StreamSupport;

/**
 * The event handler for the bot
 */
class BotListener extends ListenerAdapter {
    private static final Logger LOGGER = Logger.getLogger(BotListener.class.getName());

    @Override
    public void onConnect(ConnectEvent event) {
        event.getBot().sendRaw().rawLine(String.format("MODE %s +B", event.getBot().getNick()));
    }

    @Override
    public void onMessage(MessageEvent event) {
        try {
            Optional.ofNullable(event.getUser()).ifPresent(user -> {
                var botConfiguration = (BotConfiguration) event.getBot().getConfiguration();

                if (botConfiguration.getAutoJoinChannels().containsKey(event.getChannel().getName()) && !botConfiguration.getIgnoredNicks().contains(user.getNick())) {
                    if (event.getMessage().startsWith(RFC2119.TRIGGER)) {
                        event.respondWith(RFC2119.parse(event.getMessage()));
                    }

                    StreamSupport.stream(LinkExtractor.builder().build().extractLinks(event.getMessage()).spliterator(), true)
                            .filter(linkSpan -> linkSpan.getType().equals(LinkType.URL))
                            .map(linkSpan -> event.getMessage().substring(linkSpan.getBeginIndex(), linkSpan.getEndIndex()))
                            .map(url -> new MetadataExtractor(url, event.getBot().getConfiguration().getLocale()).extractMetadata())
                            .filter(s -> s != null && !s.isEmpty())
                            .forEach(response -> {
                                event.respondWith(response);
                                LOGGER.info(String.join(StringUtils.SPACE, botConfiguration.getNetworkName(),
                                        event.getChannel().getName(), user.getNick(), event.getMessage(), response));
                            });
                }
            });
        } catch (RuntimeException exception) {
            LOGGER.log(Level.SEVERE, null, exception);
        }
    }

    @Override
    public void onInvite(InviteEvent event) {
        if (event.getBot().getConfiguration().getAutoJoinChannels().containsKey(event.getChannel())) {
            event.getBot().send().joinChannel(event.getChannel());
        }
    }

    @Override
    public void onNotice(NoticeEvent event) {
        Optional.ofNullable(((BotConfiguration) event.getBot().getConfiguration()).getNickServNick())
                .ifPresent(nickServNick -> {
            Optional.ofNullable(((BotConfiguration) event.getBot().getConfiguration()).getNickServResponse())
                    .ifPresent(nickServResponse -> {
                Optional.ofNullable(((BotConfiguration) event.getBot().getConfiguration()).getNickServTrigger())
                        .ifPresent(nickServTrigger -> {
                    if (event.getUserHostmask().getNick().equals(nickServNick)
                            && event.getMessage().contains(nickServTrigger)) {
                        event.getBot().send().message(nickServNick, nickServResponse);
                    }
                });
            });
        });
    }
}
