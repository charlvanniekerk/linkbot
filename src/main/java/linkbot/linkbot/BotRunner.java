package linkbot.linkbot;

import org.pircbotx.PircBotX;
import org.pircbotx.exception.IrcException;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Runnable to start the bot inside a thread
 */
class BotRunner implements Runnable {
    private static final Logger LOGGER = Logger.getLogger(BotRunner.class.getName());

    private PircBotX pircBotX;

    BotRunner(PircBotX pircBotX) {
        this.pircBotX = pircBotX;
    }

    public void run() {
        try {
            pircBotX.startBot();
        } catch (IOException | IrcException exception) {
            LOGGER.log(Level.SEVERE, "An exception occurred.", exception);
        }
    }
}
