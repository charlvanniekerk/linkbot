FROM gradle:jdk19 as build
RUN ["mkdir", "/tmp/linkbot"]
WORKDIR /tmp/linkbot
COPY . .
RUN ["gradle", "jar"]

FROM openjdk:19
WORKDIR /opt
COPY --from=build /tmp/linkbot/build/libs/linkbot.jar .
CMD ["java", "-jar", "linkbot.jar"]
